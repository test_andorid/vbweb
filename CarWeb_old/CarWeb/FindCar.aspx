﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="FindCar.aspx.vb" Inherits="CarWeb._Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="search_content_box">
        <h1 class="title">Search</h1>
        <p class="page_instructions">Please enter in your search criteria below: <a href="show_helpfb5f.html?a=574&amp;l=2" class='lightUpLink' onclick="return false;">
            <img src="Images/help.gif" alt="" class='help_icon' /></a></p>
    </div>
    <form action="Default.aspx" method="get">
        <div>
            <input type='hidden' name='a' value='19' />
            <div class="search_content_box">
                <div class="center">
                <input type='text' id='search_text' name='b[search_text]' size='60' maxlength='80' class="field" placeholder="Keyword(s) Search: " />
                <select class="field" name="b[whole_word]">
                    <option value="1">match whole words</option>
					<option value="0" selected="selected">partial word match</option>
					<option value="2">by item # only</option>
                </select>
                <select class="field" name="b[classified_auction_search]">
                    <option value="0" selected="selected">auctions and advertizements</option>
						<option value="1">advertizements only</option>
						<option value="2">auctions only</option>
						<option value="3">only auctions using Buy Now</option>
                </select>
                </div>
                <div style="text-align:center; margin: 5px auto 10px auto;">
                <label><input type='checkbox' id='ending_today' name='b[ending_today]' />Only show adverts ending in the next 24 hours</label>
                </div>
                <h1 class="subtitle">by category: </h1>
                <div class="row_odd">
                <label class="field_label spacer"></label>
                <select name="c" class="field" id="adv_searchCat">
                    <option value="0" selected="selected">All Categories</option>
 	 	            <option value="15">Cars</option>
 	 	            <option value="315">4x4 Pick-up</option>
 	 	            <option value="309">Oldtimers</option>
 	 	            <option value="310">Motobikes</option>
 	 	            <option value="305">Bus/minivan</option>
 	 	            <option value="306">Construction equipment</option>
 	 	            <option value="304">Trucks</option>
 	 	            <option value="317">&nbsp;&nbsp;&nbsp;Tipper trucks</option>
 	 	            <option value="313">Trailers</option>
 	 	            <option value="307">Car parts</option>
 	 	            <option value="312">Truck parts</option>
 	 	            <option value="318">Wanted</option>
 	 	            <option value="311">Others</option>
                </select>
                <input type='hidden' name='b[subcategories_also]' value='1' />
                </div>
                <div class="row_even">
                <label class="field_label spacer"></label>						
                &nbsp;&nbsp;&nbsp;&nbsp;<label><input type='checkbox' id='search_titles' name='b[search_titles]' value='1' checked='checked' /> search titles</label>
                </div>
                <div class="row_odd">
                <label class="field_label spacer"></label>
                &nbsp;&nbsp;&nbsp;&nbsp;<label><input type='checkbox' id='search_descriptions' name='b[search_descriptions]' value='1' checked='checked' />and description</label>
                </div>
                <h1 class="subtitle">by price range:</h1>
                <div class="row_even">
                <label class="field_label spacer"></label>
                <label style="white-space: nowrap;">low limit:<input id="by_price_lower" name='b[by_price_lower]' size='15' maxlength='15' class="field" /></label>
                <label style="white-space: nowrap;">high limit:<input id="by_price_higher" name='b[by_price_higher]' size='15' maxlength='15' class="field" /></label>
                </div>
            </div>
        </div>
        <div class="clr"></div>
        <div id="catQuestions" style="display: none;"></div>
        <div class="clr"></div>
        <div class="search_content_box">
            <h1 class="subtitle">Location:</h1>
            <div class="row_odd">
                <label class="field_label">Regions:</label>
                <div class="region_selector_wrapper_b_search_location_" style="display: inline-block;">
                <div class="region_selector region_wrapper_1_b_search_location_" >
                    <label class="field_label region_label">Country</label><br />
                    <select name="b[search_location][1]" class="field region_level_1_b_search_location_">
                        <option value=""></option>
                        <option value="1">United States</option>
                        <option value="5">American Samoa</option>
                        <option value="15">Austria</option>
                        <option value="22">Belgium</option>
                        <option value="34">Bulgaria</option>
                        <option value="38">Cameroon</option>
                        <option value="39">Canada</option>
                        <option value="45">China</option>
                        <option value="53">Cote D'Ivoire</option>
                        <option value="58">Denmark</option>
                        <option value="63">Ecuador</option>
                        <option value="74">France</option>
                        <option value="79">Gabon</option>
                        <option value="82">Germany</option>
                        <option value="83">Ghana</option>
                        <option value="106">Italy</option>
                        <option value="108">Japan</option>
                        <option value="121">Liberia</option>
                        <option value="139">Mexico</option>
                        <option value="151">Netherlands</option>
                        <option value="160">Northern Mariana Islands</option>
                        <option value="171">Poland</option>
                        <option value="172">Portugal</option>
                        <option value="176">Romania</option>
                        <option value="196">Spain</option>
                        <option value="200">Sudan</option>
                        <option value="216">Turkey</option>
                        <option value="222">United Arab Emirates</option>
                        <option value="223">United Kingdom</option>
                    </select>
                </div>
                </div>
                <div class="region_selector_placeholders_b_search_location_" style="display: inline-block;">
                </div>
            </div>
            <h1 class="subtitle">Additional Criteria:</h1>
            <div class="row_even">
                <label class="field_label">by brand:</label>
                <div class="multiselect">
                <ul>
                    <li><label><input type="checkbox" name="b[optional_field_1][]" value="Adria"  /> Adria</label></li>
                    <li><label><input type="checkbox" name="b[optional_field_1][]" value="Alfa Romeo"  /> Alfa Romeo</label></li>
                </ul>
                <div class="clr"></div>
                <input type="checkbox" class="other_dummy_checkbox" />  of <input type="text" name="b[optional_field_1][other]" class="field" />							
                </div>
            </div>
            <div class="row_odd">
                <label class="field_label">by optional field 6:</label>
                <input type='text' name='b[optional_field_6]' id='optional_field_6' class="field" />
            </div>
        </div>
        <div class="center">
            <input type='submit' value='Search' name='b[search]' class='button' />
        </div>
    </form>
</asp:Content>
