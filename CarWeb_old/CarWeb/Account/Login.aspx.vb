﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Web.Security
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security

Public Class Login
    Inherits Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
       
    End Sub
    Protected Sub LogIn(sender As Object, e As EventArgs)
        Dim userId As String = ""
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("checkLogin")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Parameters.AddWithValue("@Username", UserName.Text)
                cmd.Parameters.AddWithValue("@Password", Password.Text)
                cmd.Connection = con
                con.Open()
                userId = cmd.ExecuteScalar()
            End Using
            If (Not String.IsNullOrEmpty(userId)) Then
                Session("User") = userId
                Response.Redirect("~/Default.aspx")

            Else
                Response.Redirect("~/Account/Login")
            End If
            con.Close()
        End Using
    End Sub
End Class
