﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Public Class UploadCSV
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            BinData()
        End If
    End Sub
    Protected Sub ImportCSV(sender As Object, e As EventArgs) Handles btnImport.Click
        'Upload and save the file
        Dim csvPath As String = Server.MapPath("~/Files/") + Path.GetFileName(FileUpload1.PostedFile.FileName)
        FileUpload1.SaveAs(csvPath)

        'Create a DataTable.
        Dim dt As New DataTable()
        dt.Columns.AddRange(New DataColumn(47) {New DataColumn("id", GetType(Long)), New DataColumn("url", GetType(String)), New DataColumn("image_origin", GetType(String)), New DataColumn("car_id_1", GetType(Long)), New DataColumn("car_id_2", GetType(Long)),
          New DataColumn("category_name", GetType(String)), New DataColumn("distribute", GetType(String)), New DataColumn("capacity", GetType(Integer)), New DataColumn("car_year", GetType(String)), New DataColumn("price", GetType(String)), New DataColumn("car_type", GetType(String)), New DataColumn("type_money", GetType(String)),
          New DataColumn("other_50", GetType(String)), New DataColumn("description", GetType(String)), New DataColumn("detail", GetType(String)), New DataColumn("other_1", GetType(String)), New DataColumn("email", GetType(String)), New DataColumn("other_2", GetType(String)), New DataColumn("other_3", GetType(String)),
          New DataColumn("phone", GetType(String)), New DataColumn("other_4", GetType(String)), New DataColumn("other_5", GetType(String)), New DataColumn("other_6", GetType(String)), New DataColumn("other_7", GetType(String)), New DataColumn("other_8", GetType(String)), New DataColumn("zipcode", GetType(String)), New DataColumn("other_9", GetType(String)),
          New DataColumn("other_10", GetType(String)), New DataColumn("other_11", GetType(String)), New DataColumn("other_12", GetType(String)), New DataColumn("other_13", GetType(String)), New DataColumn("other_14", GetType(String)), New DataColumn("other_15", GetType(String)), New DataColumn("other_16", GetType(String)), New DataColumn("other_17", GetType(String)),
          New DataColumn("other_image_1", GetType(String)), New DataColumn("other_image_2", GetType(String)), New DataColumn("other_image_3", GetType(String)), New DataColumn("other_image_4", GetType(String)), New DataColumn("detail_other_1", GetType(String)), New DataColumn("detail_other_2", GetType(String)), New DataColumn("detail_other_3", GetType(String)),
          New DataColumn("detail_other_4", GetType(String)), New DataColumn("detail_other_5", GetType(String)), New DataColumn("detail_other_6", GetType(String)), New DataColumn("detail_other_7", GetType(String)), New DataColumn("detail_other_8", GetType(String)), New DataColumn("detail_other_9", GetType(String))
          })
        'Read the contents of CSV file.
        Dim csvData As String = File.ReadAllText(csvPath)

        'Execute a loop over the rows.
        For Each row As String In csvData.Split(ControlChars.CrLf)
            If Not String.IsNullOrEmpty(row) Then
                dt.Rows.Add()
                Dim i As Integer = 0

                'Execute a loop over the columns.
                For Each cell As String In row.Split(";"c)
                    dt.Rows(dt.Rows.Count - 1)(i) = cell
                    i += 1
                Next
            End If
        Next

        Dim consString As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As New SqlConnection(consString)
            Using sqlBulkCopy As New SqlBulkCopy(con, SqlBulkCopyOptions.TableLock, Nothing)
                'Set the database table name.
                sqlBulkCopy.DestinationTableName = "dbo.csv"
                sqlBulkCopy.BatchSize = dt.Rows.Count
                con.Open()
                sqlBulkCopy.WriteToServer(dt)
                con.Close()
                sqlBulkCopy.Close()
            End Using
        End Using
       
    End Sub
    Protected Sub BinData()
        Dim constr As String = ConfigurationManager.ConnectionStrings("constr").ConnectionString
        Using con As New SqlConnection(constr)
            Using cmd As New SqlCommand("GetAllCSV")
                cmd.Connection = con
                cmd.CommandType = CommandType.StoredProcedure
                Using sda As New SqlDataAdapter(cmd)
                    Dim dt As New DataTable()
                    sda.Fill(dt)
                    GridView1.DataSource = dt
                    GridView1.DataBind()
                End Using
            End Using
        End Using
    End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BinData()
    End Sub
End Class

