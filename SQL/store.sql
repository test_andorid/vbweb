USE [car]
GO
/****** Object:  StoredProcedure [dbo].[Insert_User]    Script Date: 4/21/2019 3:11:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure SelectUser  
as     
Begin    
Select * from [car].[dbo].[user];    
End  
  
--Insert and Update User  
Create Procedure AddOrUpdateUsers
(
@Id integer,       
@username nvarchar(50),    
@pass nvarchar(50),    
@role nchar(10),    
@email nvarchar(50),
@phone nvarchar(15),
@is_active nchar(1),
@Action varchar(10)  
)    
As    
Begin    
if @Action='Insert'    
Begin    
 Insert into [car].[dbo].[user](username,pass,role,is_active,email,phone,create_at,update_at) values(@username,@pass,@role,'0',@email, @phone, GETDATE(), GETDATE());    
End    
if @Action='Update'    
Begin    
 UPDATE [car].[dbo].[user] set username=@username,pass=@pass,role=@role,is_active=@is_active,email =@email,phone =@phone where [car].[dbo].[user].id=@Id;    
End
End  
  
--Delete Employee  
Create Procedure DeleteUsear
(    
 @Id integer    
)    
as     
Begin    
 Delete [car].[dbo].[user] where [car].[dbo].[user].id=@Id;  
End