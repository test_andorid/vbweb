---ceate db car------
CREATE DATABASE car;
----create table user-----
USE [car]
GO

/****** Object:  Table [dbo].[user]    Script Date: 4/21/2019 3:25:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[user](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](50) NULL,
	[pass] [nvarchar](50) NULL,
	[role] [nchar](10) NULL,
	[is_active] [nchar](1) NULL,
	[create_at] [datetime] NULL,
	[update_at] [datetime] NULL,
	[email] [nvarchar](50) NULL,
	[phone] [nvarchar](15) NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
----create [classifieds]------
USE [car]
GO

/****** Object:  Table [dbo].[classifieds]    Script Date: 4/28/2019 9:24:33 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[classifieds](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[seller] [bigint] NOT NULL,
	[live] [int] NULL,
	[title] [text] NULL,
	[date] [bigint] NULL,
	[description] [text] NULL,
	[precurrency] [nvarchar](252) NULL,
	[price] [numeric](10, 2) NULL,
	[postcurrency] [nvarchar](252) NULL,
	[conversion_rate] [float] NULL,
	[price_applies] [nvarchar](4) NULL,
	[image] [int] NULL,
	[offsite_videos_purchased] [bigint] NULL,
	[additional_regions_purchased] [bigint] NULL,
	[category] [bigint] NULL,
	[duration] [bigint] NULL,
	[location_city] [text] NULL,
	[location_state] [text] NULL,
	[location_country] [text] NULL,
	[location_zip] [nvarchar](10) NULL,
	[ends] [bigint] NULL,
	[search_text] [text] NULL,
	[viewed] [bigint] NULL,
	[responded] [int] NULL,
	[forwarded] [int] NULL,
	[bolding] [int] NULL,
	[better_placement] [bigint] NULL,
	[featured_ad] [int] NULL,
	[featured_ad_2] [int] NULL,
	[featured_ad_3] [int] NULL,
	[featured_ad_4] [int] NULL,
	[featured_ad_5] [int] NULL,
	[attention_getter] [int] NULL,
	[attention_getter_url] [text] NULL,
	[expiration_notice] [int] NULL,
	[expiration_last_sent] [bigint] NULL,
	[sold_displayed] [money] NULL,
	[business_type] [int] NULL,
	[optional_field_1] [text] NULL,
	[optional_field_2] [text] NULL,
	[optional_field_3] [text] NULL,
	[optional_field_4] [text] NULL,
	[optional_field_5] [text] NULL,
	[optional_field_6] [text] NULL,
	[optional_field_7] [text] NULL,
	[optional_field_8] [text] NULL,
	[optional_field_9] [text] NULL,
	[optional_field_10] [text] NULL,
	[one_votes] [bigint] NULL,
	[two_votes] [bigint] NULL,
	[three_votes] [bigint] NULL,
	[vote_total] [bigint] NULL,
	[email] [nvarchar](255) NULL,
	[expose_email] [int] NULL,
	[phone] [nvarchar](50) NULL,
	[phone_2] [nvarchar](50) NULL,
	[fax] [nvarchar](50) NULL,
	[filter_id] [bigint] NULL,
	[mapping_address] [nvarchar](50) NULL,
	[mapping_city] [text] NULL,
	[mapping_state] [text] NULL,
	[mapping_country] [text] NULL,
	[mapping_zip] [nvarchar](10) NULL,
	[paypal_id] [text] NULL,
	[renewal_length] [datetime] NULL,
	[optional_field_11] [text] NULL,
	[optional_field_12] [text] NULL,
	[optional_field_13] [text] NULL,
	[optional_field_14] [text] NULL,
	[optional_field_15] [text] NULL,
	[optional_field_16] [text] NULL,
	[optional_field_17] [text] NULL,
	[optional_field_18] [text] NULL,
	[optional_field_19] [text] NULL,
	[optional_field_20] [text] NULL,
 CONSTRAINT [PK_classifieds] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


